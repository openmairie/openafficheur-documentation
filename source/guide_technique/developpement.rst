.. _developpement:

#############
Développement
#############

.. contents::

============
Introduction
============

Cette section vise à décrire brièvement les éléments techniques permettant de comprendre les éléments logiciels spécifiques à cette application. Les éléments génériques openMairie sont à consulter sur la documentation openMairie.


=========
Stratégie
=========

Ce logiciel a été développé de façon assez pragmatique, c'est à dire souvent rustique. La volonté de rester en phase avec le coeur openMairie a cependant poussé à utiliser des approches qui devraient laisser le code assez évolutif, et faciliter une exploitation industrielle.



Test automatique 
----------------

Il n'y a pas de test automatique.



=================
Modèle de données
=================

Diagramme relationnel
---------------------
Voici pour mémoire les tables openMairie [1]_


.. figure:: erd_om.png

   ERD des tables openMairie


*Description des tables* :

* ``om_collectivite``:	Organisation à laquelle est lié le paramétrage (possibilité de partage collectivité / sous-collectivité)
* ``om_dashboard``:	Paramétrage du tableau de bord par profil
* ``om_droit``:	Droits accordés aux profils
* ``om_etat``:	Editions - Paramétrage des états (équivalents aux lettre-type)
* ``om_lettretype``:	Editions - Paramétrage des lettre-types (équivalents aux états)
* ``om_logo``:	Editions -Paramétrage des logos de lettre-types et états
* ``om_parametre``:	Paramétrage de l'application
* ``om_profil``: Profils proposés aux utilisateurs, conditionnant les droits et le tableau de bord
* ``om_requete``:	Editions - Paramétrage des requêtes utilisées par les lettre-types et les états
* ``om_sig_xxx``:	SIG - inutilisées pour openAfficheur 
* ``om_sousetat``:	Editions - Etats (tableaux) utilisés par les lettre-type ou états
* ``om_utilisateur``:	Utilisateurs locaux ou synchronisés depuis l'annuaire d'entreprise
* ``om_widget``: Widgets pour les tableaux de bord des profils


Voici les tables métier: [1]_

.. figure:: erd_oaf.png

    ERD des tables openAfficheur

*Description des tables* :

* ``afficheur``: Société d'affichage publicitaire, par vocation ou opportunisme 
* ``arrondissement``: Arrondissements des voies dans la commune.
* ``dispositif``: Dispositif d'affichage
* ``domanialite``: Type de propriétaire d'un site
* ``dpdc``: indice annuel de révision des loyers
* ``facture``: facture de TLPE ou de DPDC
* ``facture_ligne``: lignes d'une facture
* ``site``: site d'affichage comportant des dispositifs
* ``tlpe``: prix unitaire de la TLPE
* ``type_dispositif``: catégorisation de dispositif
* ``type_installation``: catégorisation de dispositif
* ``voie``: voie référentielle, permettant un libellé unique et une facilité de géolocalisation
* ``voie_arrondissement``: table de lien entre *voie* et *arrondissement*


.. [1] Diagrammes produits avec `DBeaver <https://dbeaver.io>`_


Dimensionnement
---------------

Les numérotations prévoient au maximum :

* 999 afficheurs
* 99 999 sites
* 9 999 999 dispositifs


==============
Particularités				
==============

Traitements
-----------

Les quelques procédures PL/SQL peuvent être remplacées par du code PHP sans trop de risque sur la performance.


Requêtes des éditions
---------------------

Les requêtes pourraient tout à fait être passées de SQL à objet.
