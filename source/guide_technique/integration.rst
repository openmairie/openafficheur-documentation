.. _integration:

###########
Intégration
###########

.. contents::

============
Introduction
============

Cette section vise à décrire brièvement les éléments techniques permettant d'installer, paramétrer, intégrer et exploiter le logiciel. Pour plus de détails sur les caractéristiques techniques, il faut consulter la section développement.


====================
Plateforme technique
====================

Ce logiciel a été développé de façon généralement ouverte, mais est optimisé, utilisé et testé sur une plateforme donnée. L'utilisation d'une autre plateforme pourrait provoquer des dysfonctionnements.


Serveur
-------

* Linux CentOS 6 à 7, Ubuntu 14 à 18
* Apache 2.2 à 2.4
* PHP 5.5 à 7.2
* PostgreSQL 9.4 à 10.2

Client 
------

* PC Windows 7
* Firefox ESR


============
Installation
============

Installation d'une application openMairie
-----------------------------------------

Consulter la documentation openMairie:  `Installation OM 4.9 <http://docs.openmairie.org/projects/omframework/fr/4.9/installation/index.html>`_

Le module PostGIS n'est pas installé ni utilisé par openAfficheur.

Une fois Apache/PHP et PostgreSQL en place, une base UTF-8 crée et paramétrée dans /dyn/database.inc.php, vous pouvez utiliser le script install.sh à la racine pour déployer la base de données initiale.

Le compte admin/admin permet une connexion administrateur sur l'appication web. Le compte cadre/cadre correspond à un utilisateur avec le profil CADRE.

Précision sur /var
------------------

Ce répertoire peut être monté hors de la racine documentaire, sa taille étant variable et ne contenant aucun code:

* /var/filestorage/ : contient les fichiers générés ou stockés par l'application
* /var/log/ : contient les fichiers journaux
* /var/tmp/ : contient les fichiers temporaires téléchargeables ou téléversés

Précision sur /bin
------------------

Ce répertoire peut être monté hors de la racine documenatire web. Il sert à rassembler les scripts SHELL d'appel au web-services REST internes, tel que la synchronisation LDAP. Ces scripts sont souvent ordonnancés à l'aide de ``cron`` sur Unix. 

===========
Paramétrage
===========

Synchronisation annuaire
------------------------

Pour bénéficier de la synchronisation de l'annuaire applicatif (om_utilisateur) avec l'annuaire d'entreprise (LDAP), on peut activer l'interface LDAP. Le filtre d'accès à l'annuaire doit être adapté à votre annuaire: `voir documentation openMairie <http://docs.openmairie.org/projects/omframework/fr/4.9/reference/parametrage.html#l-annuaire-ldap>`_
