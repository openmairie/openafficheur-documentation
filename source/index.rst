.. openafficheur documentation master file.

==========================
openAfficheur
==========================

.. note::

    Cette création est mise à disposition selon le Contrat Paternité-Partage des
    Conditions Initiales à l'Identique 2.0 France disponible en ligne
    http://creativecommons.org/licenses/by-sa/2.0/fr/ ou par courrier postal à
    Creative Commons, 171 Second Street, Suite 300, San Francisco,
    California 94105, USA.

**openAfficheur** est un logiciel libre, `figurant au catalogue openMairie <http://www.openmairie.org/catalogue/openafficheur>`_, qui permet de recenser et facturer les dispositifs des afficheurs. La facturation porte sur la Taxe Locale sur la Publicité Extérieure (TLPE) et les loyers des dispositifs situés sur le Domaine Privé Des Communes (DPDC).

Cette documentation a pour but de guider les utilisateurs sur l'usage de ce logiciel et d'informer les techniciens sur les caractéristiques d'intégration et développement.

Vous pouvez proposer de contribuer à corriger ou enrichir cette documentation, depuis GitHub.

Bonne lecture et n'hésitez pas à venir discuter du projet avec la communauté à l’adresse suivante : https://communaute.openmairie.org/c/autres-applications-openmairie



Manuel de l'utilisateur
=======================

.. toctree::

   manuel_utilisateur/index.rst


Guide technique
===============

.. toctree::

   guide_technique/index.rst



Contributeurs d'openAfficheur
=============================
Par ordre alphabétique inverse

* Julien Sorelli
* Laurent Groleau
