.. _fonctionnalites:

###################
Les fonctionnalités
###################

.. contents::

******************************
Les widgets du tableau de bord
******************************

Les cadres affichés sur le tableau de bord, sont appelés *widget*. C'est le profil (AGENT, CADRE ...) associé à  l'utilisateur qui conditionne les *widgets* qui sont affichés et leur position.

Il n'y a pas envore de *widgets* disponibles dans openAfficheur.

*****************************************
Recensement des dispositifs publicitaires
*****************************************

Pour l'ergonomie des listes et formulaires, consulter la section ':ref:`ergonomie`'.

==========
Afficheurs
==========

La fiche *afficheur* permet d'identifier un afficheur :

* *code*: identifiant unique pour openAfficheur, automatiquement attribué, débutant à A001
* *raison sociale*: raison sociale éventuellement complétée de l'activité ciblée, par exemple "micro-affichage"
* *siret*: n° SIRET sur 14 chiffres, identifiant national unique référencé par l'INSEE
* *tiers*: n° de tiers unique pour le Système d'Information financier
* *nom, courriel et téléphone du contact*: coordonnées de la personne contact chez l'afficheur
* *date et identifiant de dernière modification*: permet de situer la dernière modification effectuée

Il n'y a pas de contrainte d'unicité du SIRET parmi les fiches afficheurs. 
Ceci afin de permettre de déclarer pour un même afficheur réel, deux fiches 
afficheur, chacune correspondant à un parc donné de dispositifs, et de facturer 
chaque parc séparément.

Depuis la fiche d'un *afficheur* on accède à la sous-liste de ses *factures* et à
celle de ses *sites*. 


=====
Sites 
=====

La fiche *site* permet d'identifier un site attribué à un afficheur, et comportant 
des dispositifs. Le site est surtout défini par une adresse et une *domanialité* 
influant sur la facturation DPDC. 

La fiche d'un *site* se compose de:

* *code*: identifiant unique pour openAfficheur, automatiquement attribué, débutant à S00001
* *afficheur*: afficheur auquel est rattaché le site
* *boite postale, voie, complément, arrondissement*: adresse du site, sans aucune contrainte d'unicité
* *parcelle*: indication pour le moment non normalisée ni exploitée informatiquement
* *domanialité*: catégorie influant sur la nécessité d'indiquer un loyer pour les dispositifs et sur la facturation DPDC
* *photo* : photo ou document PDF contenant une série de photos décrivant le site et ses dispositifs
* *date et identifiant de dernière modification* : permet de situer la dernière modification effectuée

Hors du contexte d'une fiche *afficheur*, depuis la fiche d'un *site* :

* on accède à la sous-liste de ses *dispositifs*
* le champ *afficheur* comporte un hyper-lien vers la fiche *afficheur*

Dans le contexte d'une fiche *afficheur*, depuis la fiche d'un *site*:

* le champ *code site* comporte un hyper-lien vers la fiche *site* avec en onglet la liste de ses *dispositifs*.

Pour ajouter un *site*, il faut partir de la fiche *afficheur*. Lors de l'ajout, la 
saisie du nom de voie fait l'objet d'une recherche progressive: à chaque touche frappée, 
une liste de voies est proposée, il faut en sélectionner une. 
Si une voie n'existe pas dans les propositions, il faut la faire ajouter au paramétrage des voies.

===========
Dispositifs
===========

La fiche *dispositif* permet d'identifier un dispositif attribué à un *site* et 
par extension à un *afficheur*. 

Le *dispositif* porte surtout les élements de base du calcul de facturation: surface
pour la TLPE et loyer de référence pour le DPDC, ainsi que les dates de pose et dépose. 
Il peut regrouper plusieurs panneaux publicitaire tant que la cohérence de facturation 
n'est pas compromise.

La fiche d'un *dispositif* se compose de:

* *code*: identifiant unique pour openAfficheur, automatiquement attribué, débutant à D0000001
* *site*: site auquel est rattaché le dispositif
* *numéro*: références chez l'afficheur adresse du site, sans aucune contrainte d'unicité
* *type de dispositif*: catégorisation influant notamment sur le calcul de la TLPE
* *type d'installation*: catégorisation sans impact direct sur la facturation
* *surface*: élément influant sur le calcul de la TLPE
* *montant et année du loyer de référence*: élément influant sur le calcul du loyer DPDC
* *date de pose et dépose*: élément influant sur le calcul de la facturation
* *date et identifiant de dernière modification* : permet de situer la dernière modification effectuée

Hors du contexte d'une fiche *site*, depuis la fiche d'un *dispositif* :

* le champ *site* comporte un hyper-lien vers la fiche *site*.


********************************************
Traitement de facturation et suivi comptable
********************************************

===========
Facturation
===========

Calcul
======

Pour lancer le calcul d'une facture, il faut *Ajouter* une facture:

* De préférence depuis la fiche *afficheur* et l'onglet *facture*
* Sinon depuis la liste des factures, via le menu *Facturation*: il faudra alors sélectionner l'afficheur ciblé

Les indications à fournir pour l'ajout sont:

* *type de facturation*: TLPE ou DPDC
* *afficheur* ciblé: si on ajouter depuis la rubrique traitement
* *date de début*: ce doit être un premier jour du mois
* *date de fin*: ce doit être un dernier jour du mois
* les dates de début et de fin doivent relever de la même année, et ne pas chevaucher les dates d'une autre facture de même type pour le même afficheur

La *facture* calculée affiche alors en plus:

* *numéro*: attribué automatiquement, avec un compteur millésimé, par type de taxation et débutant à 1 (2018-DPDC-0001)
* *raison sociale et SIRET facturés*: ne seront pas impacté par une modification de la fiche afficheur
* *date de facturation*
* *montant facturé*
* *date et identifiant de dernière modification* : permet de situer la dernière modification effectuée

Le formulaire *facture* propose l'action de suppression pour laisser le temps 
des vérifications. Cette action n'est disponible que tant qu'une nouvelle 
facture n'est pas ajoutée pour le même type de taxation et la même année. 
Le numéro libéré sera ré-utilisé. 

Hors du contexte d'une fiche *afficheur*, depuis la fiche d'une *facture* :

* on accède à la sous-liste de ses *lignes*
* le champ *afficheur* comporte un hyper-lien vers la fiche *afficheur*

Dans le contexte d'une fiche *afficheur*, depuis la fiche d'une *facture* :

* le champ *numéro* comporte un hyper-lien vers la fiche *facture* avec en onglet la liste de ses *lignes*.


Export PDF et CSV
=================

Pour communiquer la facture en format PDF au service comptable ou au redevable, 
il convient d'ouvrir le formulaire de la facture et de cliquer sur l'action `
`editer la Facture``.

Pour communiquer un détail de facturation facilement analysable, il est préférable 
d'exporter les lignes de facture au format CSV:

* Cliquer sur le menu ``Facturation > Export Lignes Factures``
* Positionnez les filtres de recherche avancée, a priori le numéro de facture exact suffit
* Lancer la recherche
* exporter le résultat en cliquant sur l'icone CSV
* importer les données dans votre logiciel traitant les fichiers CSV (Calc, Excel, Editeur de texte, ...) 


===============
Suivi Comptable
===============

Depuis le formulaire de *facture*, on a accès à l'action *Modifier*. Trois champs
apparaissent alors modifiables, ils servent à effectuer le suivi comptable:

* *mouvement financier*: numéro du titre de recette émis ou de la quittance de régie, non normalisé pour le moment
* *montant annulé*: permet de tenir compte d'une régularisation hors facture openAfficheur
* *état solde*: ``emis`` ou ``Soldé`` 

