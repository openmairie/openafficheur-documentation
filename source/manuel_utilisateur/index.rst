.. _manuel_utilisateur:

#######################
Manuel de l'utilisateur
#######################

.. toctree::
    :numbered:
    :maxdepth: 3

    ergonomie/index.rst
    fonctionnalites/index.rst
    parametres/index.rst
