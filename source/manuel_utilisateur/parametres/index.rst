.. _parametrage:

###########
Paramétrage
###########

.. contents::

************
Introduction
************

Les paramètres d'installation sont décrits dans le manuel d'intégration.
Cette section décrit les paramètres accessibles depuis l'interface 
utilisateur. Ils sont répartis en deux catégories:

* menu Paramétrage: 

  - paramétrage purement métier hormis  ``Requêtes`` et ``Sous-état`` qui nécessitent une expertise technique
  - modifiable par les utilisateurs du profil SUPER-UTILISATEUR
* menu Administration: 

  - paramétrage technique qui peut altérer fortement le fonctionnement
  - modifiable   
  
    + partiellement par les utilisateurs du profil SUPER-UTILISATEUR
    + entièrement par ceux du profil ADMINISTRATEUR


******************************
Menu Paramétrage hors Editions
******************************

=========
Typologie
=========

Listes de références utilisées dans les fiche Site et Dispositif.

Domanialité
===========

Catégorisation des sites conditionnant la perception d'un loyer "DPDC":

* Sur un site de domanialité impliquant la perception de loyer, les dispositifs doivent indiquer un loyer de référence et une année de référence
* Sur un site de domanialité n'impliquant pas la perception de loyer, les dispositifs ne doivent pas indiquer de loyer de référence ni d'année de référence


Type de dispositif
==================

Catégorisation des dispositifs conditionnant la façon dont ils sont
facturés en TLPE. Cette catégorisation facilite aussi l'identification 
du dispositif et peut avoir un impact juridique. Les valeurs sont libres,
la surface par défaut n'est pas utilisée pour le moment.

Type d'installation
===================

Catégorisation de la façon dont les dispositifs sont installés, pouvant
avoir un impact juridique et facilitant leur localisation. Les valeurs 
sont libres et n'influent pas sur la facturation.

=================
Référentiel Voies
=================

Voie
====

Liste des voies et des arrondissements où elles sont présente à récupérer
depuis un référentiel local ou le `répertoire national FANTOIR <https://www.data.gouv.fr/fr/datasets/fichier-fantoir-des-voies-et-lieux-dits/>`_.

Arrondissement
==============

Liste des arrondissements.

============
Tarification
============

TLPE
====

Il faut conserver les 4 enregistrments fournis au départy et modifier uniquement 
le montant du *tarif annuel* chaque début d'année:

* *code*: ce code est utilisée par le calcul de facturation
* *description*: cette description rappelle de quelle tarification il s'agit
* *tarif annuel*: montant annuel en euro pour un mètre carré, tel que défini par la commune

On peut se référer aux tarifs publiés sur `service-public.gouv.fr <https://www.service-public.fr/professionnels-entreprises/vosdroits/F22591>`_

DPDC
====

Il faut indiquer ici, le pourcentage annuel de révision des loyers, pour chaque année
entre celle du plus ancien loyer de référence de dispositif et l'année actuelle:

* *année*: c'est l'année de révision qui doit être indiquée
* *description*: description de l'indice retenu
* *pourcentage*: pourcentage qui sera appliqué aux loyers pour l'année de révision par rapport à la précédente

On peut par exemple prendre l'`indice des prix à la consommation <https://www.insee.fr/fr/statistiques/serie/001761313>`_ 




***************************
Menu Paramétrage - Editions
***************************

Le paramétrage des éditions d'openAfficheur est identique à celui d'opeMairie. 
Les éditions obligatoires sont les deux lettre-type d'identifiant *facture_dpdc* 
et *facture_tlpe* avec leur requête et leurs sous-états associés.

Pour modifier ces éléments ou en créer d'autres, vous pouvez vous référer à la
section `éditions <https://docs.openmairie.org/projects/omframework/fr/4.9/usage/administration/index.html#les-editions>`_ de la documentation openMairie 4.9.


*******************
Menu Administration
*******************

=========================
Eléments d'administration
=========================

Collectivité
============

Permet de définir plusieurs collectivités utilisatrices, par exemple dans un 
contexte d'intercommunalité. Chaque collectivité peut ainsi déclarer ses 
utilisateurs, ses paramètres et ses modèles de document. Pour plus d'informations,
se référer à la `documentation openMairie <http://docs.openmairie.org/projects/omframework/fr/4.9/reference/acces.html#la-multi-collectivite>`_.


Paramètre
=========

Les paramètres permettent d'influer sur le comportement général de l'application.

+-------------------------------+-------------------------------------------------------------+
| **PARAMETRE**                                                                               |
+-------------------------------+-------------------------------------------------------------+
| **exemple de valeur**         | **description**                                             |
+===============================+=============================================================+
| *ville*                                                                                     | 
+-------------------------------+-------------------------------------------------------------+
| LIBREVILLE                    | |  Nom de la collectivité, affiché en haut à droite         |
|                               | |                                                           |
+-------------------------------+-------------------------------------------------------------+
| *option__autoriser_suppression_facture_sans_controle*                                       | 
+-------------------------------+-------------------------------------------------------------+
| non                           | |  Autorise de supprimer une facture qui n'est pas la       |
|                               | |  dernière tant que le mouvement financier n'est pas       |
|                               | |  renseigné, créant ainsi une rupture de numérotation      |
+-------------------------------+-------------------------------------------------------------+

========================
Gestion des utilisateurs
========================

Profil
======

Un profil définit les droits d'un utilisateur et son tableau de bord.


Droit
=====

Chaque enregistrement affecte une permission à un profil. Dans opeAfficheur, 
les profils héritent des droits des profils de *hiérarchie* inférieure.

Pour le nommage des permissions, on utilise les `règles <http://docs.openmairie.org/projects/omframework/fr/4.9/reference/acces.html#les-regles>`_ de base openMairie.

Utilisateur
===========

Descritpion de l'utilisateur, qui peut être créé sur l'applcation ou récupéré 
d'un annuaire d'entreprise. L'utilisateur sera associé à un profil pour définir 
ses droits.

Annuaire
========

Ecran de synchronisation avec l'annuaire d'entreprise *LDAP* si paramétré.

================
Tableaux de bord
================

Pour le fonctionnement général du tableau de bord, se référer à la section `tableaux de bord de la documentation 
openMairie 4.9 <http://docs.openmairie.org/projects/omframework/fr/4.9/usage/administration/index.html#les-tableaux-de-bord>`_.


================
Options avancées
================

Import
======
Inexploité pour le moment dans openAfficheur. Ce pourra être un moyen de prendre 
en compte les déclarations de TLPE des afficheurs. actuellemetn 


Générateur
==========

Voir la section `générateur de la documentation openMairie 4.9 <http://docs.openmairie.org/projects/omframework/fr/4.9/usage/generation/index.html>`_.


